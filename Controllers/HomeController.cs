﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using LearCore.Models;
using LearCore.GeneralFunc;

namespace LearCore.Controllers
{
    public class HomeController : Controller
    {
        private String pStrGlobabProfile;
        private int pIntAge;

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetDataAdd()
        {
            try
            {
                var iCLsGeneralDunc = GeneralFunc.GeneralFuncClass.GetDataAdd(1, 2);

                // if (2 == 2)
                // {
                //     throw new Exception("Data tidak sesuai");
                // }

                return Json(new { status = true, remarks = "OK", Data = iCLsGeneralDunc });// new GeneralFunc().;

            }
            catch (Exception ex)
            {
                tbl_error_log.InsertDataLogException(ex);
                return Json(new { status = false, remarks = "Data bisa di proses" });// new GeneralFunc().;

            }
        }

        public decimal GetDataDivide()
        {
            var iCLsGeneralDunc = new GeneralFunc.GeneralFuncClass().GetDataDivide(10, 3);
            return iCLsGeneralDunc;// new GeneralFunc().;
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
